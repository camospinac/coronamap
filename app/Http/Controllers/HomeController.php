<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Http;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {

       
        $respuesta = Http::get('https://api.covid19api.com/summary');
        $covid = $respuesta->json();
        return view('dashboard', compact('covid'));
    }
}
