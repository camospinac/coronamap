<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('ide')->unique();
            $table->string('nombreuno');
            $table->string('nombredos')->nullable();
            $table->string('apellidopat');
            $table->string('apellidomat')->nullable();
            $table->bigInteger('numcelu');
            $table->string('sintomas');
            $table->bigInteger('edad');
            $table->string('barrio');
            $table->string('tipodoc');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
