@extends('layouts.app', ['activePage' => 'profile', 'titlePage' => __('User Profile')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('profile.update') }}" autocomplete="off" class="form-horizontal">
            @csrf
            @method('put')

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Editar perfil') }}</h4>
                <p class="card-category">{{ __('Información de la empresa') }}</p>
              </div>
              <div class="card-body ">
                @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif

                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('NIT de la empresa') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('nit') ? ' has-danger' : '' }}">
                      <input disabled class="form-control{{ $errors->has('nit') ? ' is-invalid' : '' }}" name="nit" id="input-nit" type="text" placeholder="{{ __('NIT') }}" value="{{ old('nit', auth()->user()->nit) }}" required="true" aria-required="true"/>
                      @if ($errors->has('nit'))
                        <span id="nit-error" class="error text-danger" for="input-nit">{{ $errors->first('nit') }}</span>
                      @endif
                    </div>
                  </div>
                </div>




                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Nombre de la empresa') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                      <input disabled class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" placeholder="{{ __('Nombre de la empresa') }}" value="{{ old('name', auth()->user()->name) }}" required="true" aria-required="true"/>
                      @if ($errors->has('name'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                      @endif
                    </div>
                  </div>
                </div>

                <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('Ciudad') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('ciudad') ? ' has-danger' : '' }}">
                    <input disabled class="form-control{{ $errors->has('ciudad') ? ' is-invalid' : '' }}" name="ciudad" id="input-ciudad" type="text" placeholder="{{ __('Ciudad') }}" value="{{ old('ciudad', auth()->user()->ciudad) }}" required="true" aria-required="true"/>
                    @if ($errors->has('ciudad'))
                      <span id="ciudad-error" class="error text-danger" for="input-ciudad">{{ $errors->first('ciudad') }}</span>
                    @endif
                  </div>
                </div>
              </div>

              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('Dirección') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('direccion') ? ' has-danger' : '' }}">
                    <input disabled class="form-control{{ $errors->has('direccion') ? ' is-invalid' : '' }}" name="direccion" id="input-direccion" type="text" placeholder="{{ __('Dirección') }}" value="{{ old('direccion', auth()->user()->direccion) }}" required="true" aria-required="true"/>
                    @if ($errors->has('direccion'))
                      <span id="direccion-error" class="error text-danger" for="input-direccion">{{ $errors->first('direccion') }}</span>
                    @endif
                  </div>
                </div>
              </div>
            

              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('Número de contacto') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('numCelular') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('numCelular') ? ' is-invalid' : '' }}" name="numCelular" id="input-numCelular" type="number" placeholder="{{ __('Número de contacto') }}" value="{{ old('numCelular', auth()->user()->numCelular) }}" required="true" aria-required="true"/>
                    @if ($errors->has('numCelular'))
                      <span id="numCelular-error" class="error text-danger" for="input-numCelular">{{ $errors->first('numCelular') }}</span>
                    @endif
                  </div>
                </div>
              </div>




                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('E-mail') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="input-email" type="email" placeholder="{{ __('E-mail') }}" value="{{ old('email', auth()->user()->email) }}" required />
                      @if ($errors->has('email'))
                        <span id="email-error" class="error text-danger" for="input-email">{{ $errors->first('email') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Guardar') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('profile.password') }}" class="form-horizontal">
            @csrf
            @method('put')

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Cambiar contraseña') }}</h4>
                <p class="card-category">{{ __('Contraseña') }}</p>
              </div>
              <div class="card-body ">
                @if (session('status_password'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status_password') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="row">
                  <label class="col-sm-2 col-form-label" for="input-current-password">{{ __('Contraseña actual') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('old_password') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('old_password') ? ' is-invalid' : '' }}" input type="password" name="old_password" id="input-current-password" placeholder="{{ __('Contraseña actual') }}" value="" required />
                      @if ($errors->has('old_password'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('old_password') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label" for="input-password">{{ __('Nueva Contraseña') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" id="input-password" type="password" placeholder="{{ __('Nueva contraseña') }}" value="" required />
                      @if ($errors->has('password'))
                        <span id="password-error" class="error text-danger" for="input-password">{{ $errors->first('password') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label" for="input-password-confirmation">{{ __('Confirme nuevamente la contraseña') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="password_confirmation" id="input-password-confirmation" type="password" placeholder="{{ __('Nueva contraseña') }}" value="" required />
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Cambiar contraseña') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection