<footer class="footer">
    <div class="container-fluid">
      <div class="copyright float-right text-dark">
        &copy;
        <script>
          document.write(new Date().getFullYear())
        </script>, Hecho con <i class="material-icons">favorite</i> por el equipo de 
        <a href="#" target="_blank">CoronaMap</a> & <a href="https://girardot.unipiloto.edu.co/" target="_blank">Universidad Piloto SAM</a> Todos los derechos reservados.
      </div>
    </div>
  </footer>