@extends('layouts.app', ['activePage' => 'table', 'titlePage' => __('Table List')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Tabla de Clientes</h4>
            <p class="card-category">Aquí se relacionan los clientes del establecimiento</p>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th>
                    ID
                  </th>
                  <th class="text-center">
                    1er Nombre
                  </th>
                  <th class="text-center">
                    2do Nombre
                  </th>
                  <th class="text-center">
                    1er Apellido
                  </th>
                  <th class="text-center">
                    2do Apellido
                  </th>
                  <th>
                    Edad
                  </th>
                  <th class="text-center">
                    N° Contacto
                  </th>
                  <th>
                    Síntomas
                  </th>
                  <th>
                    Barrio
                  </th>
                  <th>
                    Acciones
                  </th>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      1103410704
                    </td>
                    <td>
                      Camilo
                    </td>
                    <td>
                      Antonio
                    </td>
                    <td>
                      Ospina
                    </td>
                    <td>
                      Cruz
                    </td>
                    <td>
                      20
                    </td>
                    <td>
                      3024042842
                    </td>
                    <td>
                      Ninguno
                    </td>
                    <td>
                      Sucre
                    </td>
                  
                    <td>
                      <div class="center-block">
                        <button class="btn btn-danger text-center ">Modificar</button>
                        <button class="btn btn-warning mt-2 text-center">Eliminar</button>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
@endsection