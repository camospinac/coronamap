@extends('layouts.app', ['activePage' => 'table', 'titlePage' => __('Table List')])

@section('content')


<div class="container" style="height: auto;">
    
    <div class="row align-items-left">
      <div class="col-12  mt-5  mr-auto">
        <a class="btn btn-danger ml-3 mt-4" href="{{ route('clients') }}">Regresar</a>
      <form class="form" method="post" action="{{url('/clients/'.$cliente->id)}}">
          {{csrf_field()}}
          {{method_field('PATCH')}}
          <div class="card card-login card-hidden mb-3">
            <div class="card-header card-header-primary ">
              <h4 class="card-title"><strong>{{ __('Registrar clientes') }}</strong></h4>
            </div>
  
                <div class="form-row col-12 mt-4">
                    <div class="col">
                        <input type="text" value="{{$cliente->tipodoc}}" name="tipodoc" id="tipodoc" class="form-control" placeholder="Tipo de Identifacion">
                    </div>
                    <div class="col">
                        <input type="number"  value="{{$cliente->ide}}" name="ide" id="ide" class="form-control" placeholder="Identificación">
                    </div>

                </div>
                <div class="form-row col-12 mt-2">
                    <div class="col">
                        <input type="text" value="{{$cliente->nombreuno}}" name="nombreuno" id="nombreuno" class="form-control" placeholder="1er Nombre">
                    </div>
                    <div class="col">
                        <input type="text" value="{{$cliente->nombredos}}" name="nombredos" id="nombredos" class="form-control" placeholder="2do Nombre">
                    </div>
                </div>
                <div class="form-row col-12 mt-2">
                    <div class="col">
                        <input type="text" value="{{$cliente->apellidopat}}" name="apellidopat" id="apellidopat" class="form-control" placeholder="1er Apellido">
                    </div>
                    <div class="col">
                        <input type="text" value="{{$cliente->apellidomat}}" name="apellidomat" id="apellidomat" class="form-control" placeholder="2do Apellido">
                    </div>
                </div>
                <div class="form-row col-12">
                    <div class="col-md-1 mt-1 ">
                        <input type="number" value="{{$cliente->edad}}" name="edad" id="edad" class="form-control" placeholder="Edad"> 
                    </div>
                    <div class="col-md-3 mt-1">
                        <input type="text" value="{{$cliente->barrio}}" name="barrio" id="barrio" class="form-control" placeholder="Barrio">
                    </div>
                    <div class="col-md-3 mt-1">
                        <input type="number" value="{{$cliente->numcelu}}" name="numcelu" id="numcelu" class="form-control" placeholder="Número de contacto">
                    </div>
                    <div class="col-md-5 mt-1">
                        <input type="text"  value="{{$cliente->sintomas}}" name="sintomas" id="sintomas" class="form-control" placeholder="Sintomas">
                    </div>
                </div>
             <div class="card-footer justify-content-center">
              <button type="submit" class="btn btn-primary btn-link btn-lg">{{ __('Modificar cliente') }}</button>
            </div> 
          </div>
        </form>
      </div>
    </div>
  </div>




@endsection