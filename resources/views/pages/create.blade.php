@extends('layouts.app', ['activePage' => 'table', 'titlePage' => __('Table List')])

@section('content')
<div class="container" style="height: auto;">
    <div class="row align-items-left">
      <div class="col-12  mt-5  mr-auto">
        <form class="form" method="POST" action="{{ route('clients') }}">
          @csrf
          <div class="card card-login card-hidden mb-3">
            <div class="card-header card-header-primary ">
              <h4 class="card-title"><strong>{{ __('Registrar clientes') }}</strong></h4>
            </div>
  
                <div class="form-row col-12 mt-4">
                    <div class="col">
                        <input type="text" name="tipodoc" id="tipodoc" class="form-control" placeholder="Tipo de Identifacion">
                    </div>
                    <div class="col">
                        <input type="number" name="ide" id="ide" class="form-control" placeholder="Identificación">
                    </div>

                </div>
                <div class="form-row col-12 mt-2">
                    <div class="col">
                        <input type="text" name="nombreuno" id="nombreuno" class="form-control" placeholder="1er Nombre">
                    </div>
                    <div class="col">
                        <input type="text" name="nombredos" id="nombredos" class="form-control" placeholder="2do Nombre">
                    </div>
                </div>
                <div class="form-row col-12 mt-2">
                    <div class="col">
                        <input type="text" name="apellidopat" id="apellidopat" class="form-control" placeholder="1er Apellido">
                    </div>
                    <div class="col">
                        <input type="text" name="apellidomat" id="apellidomat" class="form-control" placeholder="2do Apellido">
                    </div>
                </div>
                <div class="form-row col-12">
                    <div class="col-md-1 mt-1 ">
                        <input type="number" name="edad" id="edad" class="form-control" placeholder="Edad"> 
                    </div>
                    <div class="col-md-3 mt-1">
                        <input type="text" name="barrio" id="barrio" class="form-control" placeholder="Barrio">
                    </div>
                    <div class="col-md-3 mt-1">
                        <input type="number" name="numcelu" id="numcelu" class="form-control" placeholder="Número de contacto">
                    </div>
                    <div class="col-md-5 mt-1">
                        <input type="text" name="sintomas" id="sintomas" class="form-control" placeholder="Sintomas">
                    </div>
                </div>
            <div class="card-footer justify-content-center">
              <button type="submit" class="btn btn-primary btn-link btn-lg">{{ __('Agregar cliente') }}</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection