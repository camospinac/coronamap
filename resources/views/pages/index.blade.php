@extends('layouts.app', ['activePage' => 'table', 'titlePage' => __('Table List')])

@section('content')


<div class="content">
  <a class="btn btn-primary ml-5 " href="{{ route('/clients/create') }}">Registrar nuevo cliente</a>
  
  @if (Session::has('mensaje'))
    <div class="alert alert-success text-center mt-2 mb-2" role="alert">
   {{ Session::get('mensaje')}}
  </div>
  
  @endif

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Tabla de Clientes</h4>
            <p class="card-category">Aquí se relacionan los clientes del establecimiento</p>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th class="text-center">T. Identificación</th>
                  <th class="text-center">ID</th>
                  <th class="text-center">1er Nombre</th>
                  <th class="text-center">2do Nombre</th>
                  <th class="text-center">1er Apellido</th>
                  <th class="text-center">2do Apellido</th>
                  <th class="text-center">Edad</th>
                  <th class="text-center">N° Contacto</th>
                  <th class="text-center">Síntomas</th>
                  <th class="text-center">Barrio</th>
                  <th class="text-center">Acciones</th>
                </thead>
                <tbody>
                  @foreach ($clientes as $cliente)
                  <tr>
                    <td class="text-center">{{$cliente->tipodoc}}</td>
                    <td class="text-center">{{$cliente->ide}}</td>
                    <td class="text-center">{{$cliente->nombreuno}}</td>
                    <td class="text-center">{{$cliente->nombredos}}</td>
                    <td class="text-center">{{$cliente->apellidopat}}</td>
                    <td class="text-center">{{$cliente->apellidomat}}</td>
                    <td class="text-center">{{$cliente->edad}}</td>
                    <td class="text-center">{{$cliente->numcelu}}</td>
                    <td class="text-center">{{$cliente->sintomas}}</td>
                    <td class="text-center">{{$cliente->barrio}}</td>
                    <td class="text-center">
                      <div class="center-block">
                        <a class="btn btn-warning text-center " href="{{url('/clients/'.$cliente->id.'/edit')}}">Modificar</a>
                      
                        <form method="post" action="{{url('/clients/'.$cliente->id)}}">
                          {{csrf_field() }}
                          {{method_field('DELETE') }}
                          <button type="submit" class="btn btn-danger mt-2 text-center" onclick="return confirm('¿Seguro desea borrar el usuario?')">Eliminar</button>
                        </form>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
@endsection