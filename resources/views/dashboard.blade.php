@extends('layouts.app', ['activePage' => 'dashboard', 'titlePage' => __('Dashboard')])

@section('content')
<div class="content">
  <h2 class="ml-3">
    Dashboard 
  </h2>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header card-header-primary">
          <h4 class="card-title ">API Rest | Estado Mundial</h4>
          <p class="card-category">Se proporcionan datos actualizados sobre el avance de la pandemia. Dataset de POSTMAN</p>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table">
              <thead class=" text-primary">
                <th class="text-center">Nuevos casos confirmados</th>
                <th class="text-center">Total confirmados</th>
                <th class="text-center">Nuevas muertes</th>
                <th class="text-center">Total muertes</th>
                <th class="text-center">Nuevos pacientes recuperados</th>
                <th class="text-center">Total pacientes recuperados</th>
              </thead>
              <tbody>
                <tr>
                  <td class="text-center">{{$covid['Global']['NewConfirmed']}}</td>
                  <td class="text-center">{{$covid['Global']['TotalConfirmed']}}</td>
                  <td class="text-center">{{$covid['Global']['NewDeaths']}}</td>
                  <td class="text-center">{{$covid['Global']['TotalDeaths']}}</td>
                  <td class="text-center">{{$covid['Global']['NewRecovered']}}</td>
                  <td class="text-center">{{$covid['Global']['TotalRecovered']}}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header card-header-primary">
          <h4 class="card-title ">API Rest | Casos Colombia</h4>
          <p class="card-category">Se proporcionan datos actualizados sobre el avance de la pandemia. Dataset de POSTMAN</p>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table">
              <thead class=" text-primary">
                <th class="text-center">Fecha actualizada</th>
                <th class="text-center">Nuevos casos confirmados</th>
                <th class="text-center">Total casos confirmados</th>
                <th class="text-center">Nuevas muertes</th>
                <th class="text-center">Total muertes</th>
                <th class="text-center">Nuevos pacientes recuperados</th>
                <th class="text-center">Total pacientes recuperados</th>
              </thead>
              <tbody>
                <tr>
                  <td class="text-center">{{$covid['Countries']['36']['Date']}}</td>
                  <td class="text-center">{{$covid['Countries']['36']['NewConfirmed']}}</td>
                  <td class="text-center">{{$covid['Countries']['36']['TotalConfirmed']}}</td>
                  <td class="text-center">{{$covid['Countries']['36']['NewDeaths']}}</td>
                  <td class="text-center">{{$covid['Countries']['36']['TotalDeaths']}}</td>
                  <td class="text-center">{{$covid['Countries']['36']['NewRecovered']}}</td>
                  <td class="text-center">{{$covid['Countries']['36']['TotalRecovered']}}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>


   

@endsection
