@extends('layouts.app', ['class' => 'off-canvas-sidebar', 'activePage' => 'register', 'title' => __('Material Dashboard')])

@section('content')
<div class="container" style="height: auto;">
  <div class="row align-items-center">
    <div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
      <form class="form" method="POST" action="{{ route('register') }}">
        @csrf

        <div class="card card-login card-hidden mb-3">
          <div class="card-header card-header-primary text-center">
            <h4 class="card-title"><strong>{{ __('Registrate') }}</strong></h4>
          </div>
          <div class="card-body ">

            <div class="bmd-form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                      <i class="material-icons">store</i>
                  </span>
                </div>
                <input type="text" name="name" class="form-control" placeholder="{{ __('Nombre de la empresa..') }}" value="{{ old('name') }}" required>
              </div>
              @if ($errors->has('name'))
                <div id="name-error" class="error text-danger pl-3" for="name" style="display: block;">
                  <strong>{{ $errors->first('name') }}</strong>
                </div>
              @endif
            </div>

            <div class="bmd-form-group{{ $errors->has('nit') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                      <i class="material-icons">payment</i>
                  </span>
                </div>
                <input type="text" name="nit" class="form-control" placeholder="{{ __('NIT de la empresa...') }}" value="{{ old('nit') }}" required>
              </div>
              @if ($errors->has('nit'))
                <div id="nit-error" class="error text-danger pl-3" for="nit" style="display: block;">
                  <strong>{{ $errors->first('nit') }}</strong>
                </div>
              @endif
            </div>

            <div class="bmd-form-group{{ $errors->has('ciudad') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                      <i class="material-icons">room</i>
                  </span>
                </div>
                <input type="text" name="ciudad" class="form-control" placeholder="{{ __('Ciudad...') }}" value="{{ old('ciudad') }}" required>
              </div>
              @if ($errors->has('ciudad'))
                <div id="ciudad-error" class="error text-danger pl-3" for="ciudad" style="display: block;">
                  <strong>{{ $errors->first('ciudad') }}</strong>
                </div>
              @endif
            </div>

            <div class="bmd-form-group{{ $errors->has('direccion') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                      <i class="material-icons">directions</i>
                  </span>
                </div>
                <input type="text" name="direccion" class="form-control" placeholder="{{ __('Direccion...') }}" value="{{ old('direccion') }}" required>
              </div>
              @if ($errors->has('direccion'))
                <div id="direccion-error" class="error text-danger pl-3" for="direccion" style="display: block;">
                  <strong>{{ $errors->first('direccion') }}</strong>
                </div>
              @endif
            </div>

            <div class="bmd-form-group{{ $errors->has('numCelular') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                      <i class="material-icons">smartphone</i>
                  </span>
                </div>
                <input type="number" name="numCelular" class="form-control" placeholder="{{ __('Número de celular...') }}" value="{{ old('numCelular') }}" required>
              </div>
              @if ($errors->has('numCelular'))
                <div id="numCelular-error" class="error text-danger pl-3" for="numCelular" style="display: block;">
                  <strong>{{ $errors->first('numCelular') }}</strong>
                </div>
              @endif
            </div>



            <div class="bmd-form-group{{ $errors->has('email') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">email</i>
                  </span>
                </div>
                <input type="email" name="email" class="form-control" placeholder="{{ __('E-mail...') }}" value="{{ old('email') }}" required>
              </div>
              @if ($errors->has('email'))
                <div id="email-error" class="error text-danger pl-3" for="email" style="display: block;">
                  <strong>{{ $errors->first('email') }}</strong>
                </div>
              @endif
            </div>
            <div class="bmd-form-group{{ $errors->has('password') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">lock_outline</i>
                  </span>
                </div>
                <input type="password" name="password" id="password" class="form-control" placeholder="{{ __('Contraseña...') }}" required>
              </div>
              @if ($errors->has('password'))
                <div id="password-error" class="error text-danger pl-3" for="password" style="display: block;">
                  <strong>{{ $errors->first('password') }}</strong>
                </div>
              @endif
            </div>
            <div class="bmd-form-group{{ $errors->has('password_confirmation') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">lock_outline</i>
                  </span>
                </div>
                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="{{ __('Confirmar contraseña...') }}" required>
              </div>
              @if ($errors->has('password_confirmation'))
                <div id="password_confirmation-error" class="error text-danger pl-3" for="password_confirmation" style="display: block;">
                  <strong>{{ $errors->first('password_confirmation') }}</strong>
                </div>
              @endif
            </div>
            <div class="form-check mr-auto ml-3 mt-3">
              <label class="form-check-label">
                <input class="form-check-input" type="checkbox" id="policy" name="policy" {{ old('policy', 1) ? 'checked' : '' }} >
                <span class="form-check-sign">
                  <span class="check"></span>
                </span>
                {{ __('Acepto las') }} <a href="#">{{ __('Politícas de Privacidad') }}</a>
              </label>
            </div>
          </div>
          <div class="card-footer justify-content-center">
            <button type="submit" class="btn btn-primary btn-link btn-lg">{{ __('Crear Cuenta') }}</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
