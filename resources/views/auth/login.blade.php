@extends('layouts.app', ['class' => 'off-canvas-sidebar', 'activePage' => 'login', 'title' => __('Material Dashboard')])

@section('content')
<div class="container" style="height: auto;">
  <div class="row align-items-center">
    <div class="col-md-9 ml-auto mr-auto mb-3 text-center">
      <h3>{{ __('Bienvenidos a CoronaMap, la aplicación que facilitará el registro de clientes en tu establecimiento.') }} </h3>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
      <form class="form" method="POST" action="{{ route('login') }}">
        @csrf

        <div class="card card-login card-hidden mb-3">
          <div class="card-header card-header-primary text-center">
            <h4 class="card-title"><strong>{{ __('Ingreso a CoronaMap') }}</strong></h4>

          </div>
          <div class="card-body">
            <div class="bmd-form-group{{ $errors->has('nit') ? ' has-danger' : '' }}">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">payment</i>
                  </span>
                </div>
                <input type="nit" name="nit" class="form-control" placeholder="{{ __('NIT...') }}" value="{{ old('nit') }}" required>
              </div>
              @if ($errors->has('nit'))
                <div id="nit-error" class="error text-danger pl-3" for="nit" style="display: block;">
                  <strong>{{ $errors->first('nit') }}</strong>
                </div>
              @endif
            </div>
            <div class="bmd-form-group{{ $errors->has('password') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">lock_outline</i>
                  </span>
                </div>
                <input type="password" name="password" id="password" class="form-control" placeholder="{{ __('Contraseña...') }}" value="{{ old('password') }}" required>
              </div>
              @if ($errors->has('password'))
                <div id="password-error" class="error text-danger pl-3" for="password" style="display: block;">
                  <strong>{{ $errors->first('password') }}</strong>
                </div>
              @endif
            </div>
          </div>
          <div class="card-footer justify-content-center">
            <button type="submit" class="btn btn-primary btn-link btn-lg">{{ __('Ingresar') }}</button>
          </div>
        </div>
      </form>

    </div>
  </div>
</div>
@endsection
